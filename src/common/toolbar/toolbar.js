/**
 * 
 * Here is the list of available toolbars 
 * 
 */

import React from 'react';
import { TopToolbar, EditButton, ShowButton, ListButton, CreateButton } from 'react-admin';
// import { ConfirmedDeleteButton } from './../actions/actions';

/**
 * 
 * The actions tho show at the Show view 
 * 
 */
export const ShowActions = ({basePath, data}) => (
  <TopToolbar>
    <CreateButton basePath={basePath} />
    <ListButton basePath={basePath} record={data} />
    <EditButton basePath={basePath} record={data} />
  </TopToolbar>
);

/**
 * 
 * The actions tho show at the Edit view 
 * 
 */
export const EditActions = ({basePath, data}) => (
  <TopToolbar>
    <ListButton basePath={basePath} record={data} />
    <ShowButton basePath={basePath} record={data} />
  </TopToolbar>
);

/**
 * 
 * The actions tho show at the Create view 
 * 
 */
export const CreateActions = ({basePath, data, record}) => (
  <TopToolbar>
    <ListButton basePath={basePath} record={data} />
  </TopToolbar>
);

