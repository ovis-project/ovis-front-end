import React from "react";
import { Create, Edit, Show, List, Datagrid, SimpleForm, SimpleShowLayout } from "react-admin";
import { CreateActions, EditActions, ShowActions } from '../toolbar/toolbar';

export const CreateLayout = props => (
  <Create actions={<CreateActions />} {...props}>
    <SimpleForm redirect="show">
      {props.fields.map((component, i) => React.cloneElement(component, { key: i }))}
    </SimpleForm>
  </Create>
);

export const EditLayout = props => (
  <Edit actions={<EditActions />} {...props}>
    <SimpleForm redirect="show">
      {props.fields.map((component, i) => React.cloneElement(component, { key: i }))}
    </SimpleForm>
  </Edit>
);

export const ShowLayout = props => (
  <Show actions={<ShowActions />} {...props}>
    <SimpleShowLayout>
      {props.fields.map((component, i) => React.cloneElement(component, { key: i }))}
    </SimpleShowLayout>
  </Show>
);

export const ListLayout = props => (
  <List {...props} title={props.title}>
    <Datagrid rowClick="show">
      {props.fields.map((component, i) => React.cloneElement(component, { key: i }))}
    </Datagrid>
  </List>
);
