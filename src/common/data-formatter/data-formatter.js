/**
 * 
 * A list of data format helper functions.
 * 
 */


import { getLicenseStatus, expirationStatuses } from './../resource/resource';

export const formatPartner = (item) => (
  item 
    ? `${item.first_name} ${item.last_name} ${item.nick_name ? '(' + item.nick_name + ')' : '' }`
    : ''
);

export const formatTruck = (item) => (
  item
    ? `${item.license_number} ${item.alias ? '(' + item.alias + ')' : '' }`
    : ''
);

export const groupLicenses = (licenses) => {
  const groups = Object.values(groupBy(licenses, 'license_type_id'));
  const filteredGroups = groups
    .map((item, i) => {
      const sorted = item.sort((a, b) => b - a);
      
      return {
        license: sorted[0],
        status: getLicenseStatus(sorted[0]) 
      };
    })
    .filter((license) => license.status !== expirationStatuses.valid);

  return filteredGroups;
}

export const groupBy = (list, key) => {
  return list.reduce(function(rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
}
