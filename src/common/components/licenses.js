import React from 'react';
import Warning from '@material-ui/icons/Warning';
import Check from '@material-ui/icons/Check';
import Error from '@material-ui/icons/Error';
import { groupLicenses } from './../data-formatter/data-formatter';
import { getExpirationStatusLabel } from './../resource/resource';
import "./licenses.css";


export const InlineLicenses = ({record}) => {
  
  const licenseGroups = groupLicenses(record.licenses);
  const licenseStatuses = licenseGroups.map((item, i) =>( 
    <li 
      key={`${record.id}-${i}`} 
      className={`license-chips__item license-chips__item--${item.status}`} 
      title={`${getExpirationStatusLabel(item.status)}-${item.license.license_type ? item.license.license_type.title : item.license_type}`}
    >
      { item.status === 4 && <Error style={{ color: "red" }} />}
      { item.status === 3 && <Error style={{ color: "orange" }} />} 
      { item.status === 2 && <Warning style={{ color: "orange" }} />}
      { item.status === 1 && <Warning style={{ color: "grey" }} />}
      <span className="license-chips__item-description">
        {item.license.license_type.title}: {getExpirationStatusLabel(item.status)}
      </span>
    </li>
   )
  );

  return (
    <ul className="license-chips">
      {licenseStatuses && licenseStatuses.length > 0 && licenseStatuses}
      {licenseStatuses && licenseStatuses.length === 0 && <Check style={{ color: "green"}} /> }
    </ul>
  );
}

InlineLicenses.defaultProps = {
  addLabel: true,
};
