import React from 'react';
import { CreateLayout, EditLayout, ShowLayout, ListLayout } from './../layouts/layouts';

/**
 * 
 * Given a list of fields creates a Resource object.
 * 
 * @param {Array<React.Component>} createFields 
 * @param {Array<React.Component>} editFields 
 * @param {Array<React.Component>} showFields 
 * @param {Array<React.Component>} listFields 
 * 
 */
export const asResource = (createFields, editFields, showFields, listFields, titles={}) => {
  const create = props => (
    <CreateLayout fields={createFields} title={titles.create} {...props} />
  );

  const edit = props => (
    <EditLayout fields={editFields} title={titles.edit} {...props} />
  );

  const show = props => (
    <ShowLayout fields={showFields} title={titles.show} {...props} />
  );

  const list = props => (
    <ListLayout fields={listFields} title={titles.list} {...props} /> 
  );

  return {
    create,
    edit,
    show,
    list
  }
}

export const getResourceTypes = () => [
  {id: 'truck_owners', name:'resources.truckOwner.truckOwners'},
  {id: 'drivers', name:'resources.driver.drivers'},
  {id: 'trucks', name:'resources.truck.trucks'}
];


export const expirationStatuses = {
  valid: 0,
  expiresSoon: 1,
  shouldRenew: 2,
  critical: 3,
  expired: 4
}

export const getExpirationStatusLabel = (number) => {
  switch(number) {
    case 0: return "Valid";
    case 1: return "Expires soon";
    case 2: return "Should renew";
    case 3: return "Critical";
    case 4: return "Expired";
    default: return "Expired";
  }
}

export const getLicenseStatus = (license) => {
  const now = new Date().getTime();
  const expirationDate = new Date (license.expiration_date).getTime();
  const day = 86400000; // day in ms
  
  if (expirationDate < now) return expirationStatuses.expired;
  if ((expirationDate - now) < day * 7) return expirationStatuses.critical;
  if ((expirationDate - now) < day * 15) return expirationStatuses.expiresSoon;
  if ((expirationDate - now) < day * 30) return expirationStatuses.shouldRenew;
  return expirationStatuses.valid;
}
