import React from 'react';
import { 
  SelectInput,
  ReferenceField, 
  FunctionField, 
  DateField, 
  DateInput, 
  ReferenceInput,
  AutocompleteInput
} from "react-admin";
import { asResource } from './../common/resource/resource';
import { formatPartner, formatTruck } from './../common/data-formatter/data-formatter';

const inputFields = [
  <ReferenceInput 
    source="license_type" 
    reference="license-types" 
    label="resources.licenseType.licenseType" 
    perPage={-1}  
  >
    <SelectInput  optionText={ (item) => item.title } />
  </ReferenceInput>,
  <DateInput source="expiration_date" label="resources.license.expirationDate" />,
  <ReferenceInput 
    source="truckowner_licensee" 
    reference="truck-owners" 
    label="resources.truckOwner.truckOwner"
    perPage={-1}
    allowEmpty   
  >
    <AutocompleteInput  optionText={ (item) => formatPartner(item.partner) } filter={{ related_resource: "truck_owners" }} />
  </ReferenceInput>,
  <ReferenceInput 
    source="driver_licensee" 
    reference="drivers" 
    label="resources.driver.driver"
    perPage={-1}
    allowEmpty   
  >
    <SelectInput  optionText={ (item) => formatPartner(item.partner) } filter={{ related_resource: "drivers" }} />
  </ReferenceInput>,
  <ReferenceInput 
    source="truck_licensee" 
    reference="trucks" 
    label="resources.truck.truck"
    perPage={-1}
    allowEmpty 
  >
    <AutocompleteInput  optionText={ (item) => formatTruck(item) } filter={{ related_resource: "trucks" }} />
  </ReferenceInput>,
];

const listFields = [
  <DateField source="expiration_date" label="resources.license.expirationDate" locales="el-GR" />,
  <ReferenceField source="license_type" reference="license-types" link="show" label="resources.licenseType.licenseType">
    <FunctionField render={(item) => `${item.id} - ${item.title}`} />
  </ReferenceField>,
  <ReferenceField source="truckowner_licensee" reference="truck-owners" link="show" label="resources.truckOwner.truckOwner">
    <FunctionField render={(item) => formatPartner(item.partner)} />
  </ReferenceField>,
  <ReferenceField source="driver_licensee" reference="drivers" link="show" label="resources.driver.driver">
    <FunctionField  render={(item) => formatPartner(item.partner)} />
  </ReferenceField>,
  <ReferenceField source="truck_licensee" reference="trucks" link="show"  label="resources.truck.truck">
    <FunctionField render={(item) => formatTruck(item)} />
  </ReferenceField>
];

const showFields = [
  ...listFields,
  <DateField source="updated_at" showTime={true} label="app.updatedAt" locales="el-GR" />,
  <DateField source="created_at" showTime={true} label="app.createdAt" locales="el-GR" />
];

const titles = {
  list: "resources.license.list",
  show: "resources.license.show",
  edit: "resources.license.edit",
  create: "resources.license.create",
};

export default asResource(inputFields, inputFields, showFields, listFields, titles);
