import partners from "./partners";
import drivers from "./drivers";
import truckOwners from "./truck-owners";
import trucks from './trucks';
import licenseTypes from './license-types';
import licenses from './licenses';

export {
  partners,
  drivers,
  truckOwners,
  trucks,
  licenseTypes,
  licenses
}