import React from 'react';
import { BooleanField, AutocompleteInput, TextField, ReferenceField, FunctionField, DateField, ReferenceInput  } from "react-admin";
import { asResource } from './../common/resource/resource';
import { formatPartner } from './../common/data-formatter/data-formatter';
import { InlineLicenses } from './../common/components';

const inputFields = [
  <ReferenceInput 
    source="partner_id" 
    reference="partners" 
    label="resources.partner.partner" 
    filter={{is_not_truckowner: "true"}}
    perPage={-1}  
  >
    <AutocompleteInput optionText={formatPartner}  />
  </ReferenceInput>
];

const listFields = [
  <TextField source="id" label="app.id" />,
  <TextField source="partner.first_name" label="resources.partner.firstName" />,
  <TextField source="partner.last_name" label="resources.partner.lastName" />,
  <TextField source="partner.nick_name" label="resources.partner.nickName" />,
  <InlineLicenses label="resources.license.licenses" />,
  <BooleanField source="isActive" label="resources.partner.isActive" />
];

const showFields = [
  ...listFields,
  <DateField source="updated_at" label="app.updatedAt" showTime={true} locales="el-GR" />,
  <DateField source="created_at" label="app.createdAt" showTime={true} locales="el-GR" />,
  <ReferenceField source="partner.id" reference="partners" label="resources.partner.partner">
    <FunctionField render={record => (`Edit record ${record.first_name} ${record.last_name}`)} />
  </ReferenceField>
];

const titles = {
  list: "resources.truckOwner.list",
  show: "resources.truckOwner.show",
  edit: "resources.truckOwner.edit",
  create: "resources.truckOwner.create",
};

export default asResource(inputFields, inputFields, showFields, listFields,titles);
