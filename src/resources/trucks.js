import React from 'react';
import { TextInput, TextField, ReferenceField, FunctionField, DateField, ReferenceInput, AutocompleteInput } from "react-admin";
import { asResource } from './../common/resource/resource';
import { formatPartner } from './../common/data-formatter/data-formatter';
import { InlineLicenses } from './../common/components';

const inputFields = [
  <TextInput source="license_number" label="resources.truck.licenseNumber" required/>,
  <TextInput source="alias" label="resources.truck.alias" />,
  (
    <ReferenceInput 
        source="owner_id" 
        reference="truck-owners" 
        label="resources.truck.owner" 
        perPage={-1}
        filterToQuery={searchText => {console.log(searchText)}}
    >
      <AutocompleteInput optionText={(item) => formatPartner(item.partner)} />
    </ReferenceInput>
  ),
  (
    <ReferenceInput 
        source="driver_id" 
        reference="drivers" 
        label="resources.truck.driver" 
        perPage={-1}
        filterToQuery={searchText => {console.log(searchText)}}
    >
      <AutocompleteInput optionText={(item) => formatPartner(item.partner)} />
    </ReferenceInput>
  )
];

const listFields = [
  <TextField source="id" label="app.id" />,
  <TextField source="license_number" label="resources.truck.licenseNumber" />,
  <TextField source="alias" label="resources.truck.alias" />,
  (
    <ReferenceField reference="truck-owners" source="owner_id" link="show" label="resources.truck.owner">
      <FunctionField  label="Owner" render={(item) => formatPartner(item.partner)} />
    </ReferenceField>
  ),
  (
    <ReferenceField reference="drivers" source="driver_id" link="show" label="resources.truck.driver">
      <FunctionField  label="Driver" render={(item) => formatPartner(item.partner)} />
    </ReferenceField>
  ),
  <InlineLicenses label="resources.license.licenses" />
];

const showFields = [
  ...listFields,
  <DateField source="updated_at" showTime={true} label="app.updatedAt" locales="el-GR" />,
  <DateField source="created_at" showTime={true} label="app.createdAt" locales="el-GR" />
];

const titles = {
  list: "resources.truck.list",
  show: "resources.truck.show",
  edit: "resources.truck.edit",
  create: "resources.truck.create",
};

export default asResource(inputFields, inputFields, showFields, listFields, titles);
