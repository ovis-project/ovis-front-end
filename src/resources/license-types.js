import React from 'react';
import { TextInput, TextField, DateField, SelectInput, SelectField, BooleanInput, BooleanField } from "react-admin";
import { asResource, getResourceTypes } from '../common/resource/resource';

const inputFields = [
  <TextInput source="title" label="resources.licenseType.title" required />,
  <TextInput source="description" label="resources.licenseType.description" />,
  <SelectInput source="related_resource" label="resources.licenseType.relatedResource" required choices={getResourceTypes()} />,
  <BooleanInput source="is_required" label="resources.licenseType.isRequired" defaultValue={true} />
];

const listFields = [
  <TextField source="id" label="app.id" />,
  <TextField source="title" label="resources.licenseType.title" />,
  <BooleanField source="is_required" label="resources.licenseType.isRequired" />,
  <TextField source="description" label="resources.licenseType.description" />,
  <SelectField source="related_resource" choices={getResourceTypes()}  label="resources.licenseType.relatedResource" />
]

const showFields = [
  ...listFields,
  <DateField source="updated_at" showTime={true}  label="app.updatedAt" />,
  <DateField source="created_at" showTime={true}  label="app.createdAt" />
];

const titles = {
  list: "resources.licenseType.list",
  show: "resources.licenseType.show",
  edit: "resources.licenseType.edit",
  create: "resources.licenseType.create",
}

export default asResource(inputFields, inputFields, showFields, listFields, titles);
