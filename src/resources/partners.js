import React from 'react';
import { TextInput, TextField, EmailField, FunctionField, DateField,  } from "react-admin";
import { asResource } from '../common/resource/resource';

const inputFields = [
  <TextInput source="first_name" required  label="resources.partner.firstName" />,
  <TextInput source="last_name" required label="resources.partner.lastName"  />,
  <TextInput source="nick_name" label="resources.partner.nickName"  />,
  <TextInput source="phone" label="resources.partner.phone" />,
  <TextInput source="email" label="resources.partner.email" />,
  <TextInput source="address" label="resources.partner.address" />
];

const listFields = [
  <TextField source="id" label="app.id" />,
  <TextField source="first_name" label="resources.partner.firstName" />,
  <TextField source="last_name" label="resources.partner.lastName" />,
  <TextField source="nick_name" label="resources.partner.nickName" />,
  <FunctionField  label="resources.partner.phone" render={record =>(
    record.phone &&
      <a href={`tel:+30${record.phone}`}>
        {record.phone}
      </a>
    )}
  />,
  <EmailField source="email" label="resources.partner.email" />
]

const showFields = [
  ...listFields,
  <TextField source="address" label="resources.partner.address" />,
  <DateField source="updated_at" showTime={true} label="app.updatedAt" locales="el-GR"/>,
  <DateField source="created_at" showTime={true} label="app.createdAt" locales="el-GR" />
];

const titles = {
  list: "resources.partner.list",
  show: "resources.partner.show",
  edit: "resources.partner.edit",
  create: "resources.partner.create",
};

export default asResource(inputFields, inputFields, showFields, listFields, titles);
