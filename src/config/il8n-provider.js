import polyglotI18nProvider from 'ra-i18n-polyglot';
import greekMessages from 'ra-language-greek';
import { gr } from './../il8n';

const allGreek = {
  ...greekMessages, 
  ...gr
};

allGreek.ra.auth.user_menu = "Μενού χρήστη";
allGreek.ra.action.export = "Εξαγωγή";
allGreek.ra.action.clear_input_value = "Εκαθάριση τιμής";
allGreek.ra.action.unselect = "Αποεπιλογή";

const i18nProvider = polyglotI18nProvider(() => allGreek, 'gr');

export default i18nProvider;
