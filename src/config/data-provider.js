/**
 *
 * Data provider
 *
 * This is a custom reach admin data provider to communicate with the
 * custom API
 *
 */

import axios from 'axios';


const dataProvider = {}

if (!process.env.REACT_APP_API_URL) {
  throw new Error("REACT_APP_API_URL is not defined.");
}

const api = `${process.env.REACT_APP_API_URL}/api`;

const makeRequest = (method, path, config="", body = undefined) => {
  const filterParams = getFilterParams(config)
  const paginationParams = getPaginationParams(config);
    
  const rawParams = [];
  filterParams && rawParams.push(filterParams);
  paginationParams && rawParams.push(paginationParams);
  
  const prefix = rawParams.length > 0 ? "?" : "";
  const filters = prefix + [...rawParams].join("&");
  const url = `${api}/${path}${filters}`;
  const params = body ? body.data : undefined

  return axios[method](url, params)
    .then((response) => {
      return response.data
    })
    .catch((err) => {
      console.error("err", err)
      return err
    });
};

dataProvider.getList = (resource, config, ...rest) => {
  return makeRequest('get', resource, config);
}

dataProvider.getMany = (resource, config, ...rest) => {
  return makeRequest('get', resource, config);
}

dataProvider.getOne = (resource, { id }) => {
  return makeRequest("get", `${resource}/${id}`);
}

dataProvider.create = (resource, data) => {
  return makeRequest("post", resource, null, data);
}

dataProvider.update = (resource, {id, ...data}) => {
  return makeRequest("put", `${resource}/${id}`, null, data);
}

dataProvider.delete = (resource, {id}) => {
  return makeRequest("delete", `${resource}/${id}`);
}



const getFilterParams = (config) => {
  let filterString = "";
  if (config && config.filter) {

    const filters = Object.keys(config.filter);

    for (let i = 0; i < filters.length; i++) {
      filterString += `${filters[i]}=${config.filter[filters[i]]}`;

      if (i < filters.length -1) {
        filterString += "&";
      }
    }
  }

  return filterString;
}

const getPaginationParams = (config) => {
  if (config && config.pagination) {
    return `per_page=${config.pagination.perPage}&page=${config.pagination.page}`;
  }
  return "";
}



export default dataProvider;
