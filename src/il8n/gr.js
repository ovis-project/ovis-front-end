const messages = {
  app: {
    title: 'Συστημα διαχείρισης οχημάτων',
    updatedAt: "Τελευταία ενημέρωση",
    createdAt: "Δημίουργήθηκε ",
    id: "Μοναδικός αριθμός"
  },
  resources: {
    partner: {
      partners: "Συνεργάτες",
      partner: "Συνεργάτης",
      firstName: "Όνομα",
      lastName: "Επώνυμο",
      nickName: "Ψευδώνυμο",
      phone: "Τηλέφωνο",
      email: "Email",
      isActive: "Είναι ενεργός",
      address: "Διεύθυνση",
      list: "Λίστα των συνεργατών",
      show: "Προβολή συνεργάτη",
      edit: "Επεξεργασία συνεργάτη",
      create: "Δημιουργία συνεργάτη"
    },
    truckOwner: {
      truckOwners: "Ιδιοκτήτες οχημάτων",
      truckOwner: "Ιδιοκτήτης οχήματος",
      list: "Λίστα των ιδιοκτήτων οχήματος",
      show: "Προβολή ιδιοκτήτη οχήματος",
      edit: "Επεξεργασία ιδιοκτήτη οχήματος",
      create: "Δημιουργία ιδιοκτήτη οχήματος"
    },
    driver: {
      drivers: "Οδηγοί",
      driver: "Οδηγός",
      list: "Λίστα των οδηγών",
      show: "Προβολή οδηγού",
      edit: "Επεξεργασία οδηγού",
      create: "Δημιουργία οδηγού"
    },
    truck: {
      trucks: "Οχήματα",
      truck: "Όχημα",
      owner: "Ιδιοκτήτης",
      driver: "Οδηγός",
      alias: "Ψευδώνυμο",
      licenseNumber: "Αριθμός πινακίδας",
      list: "Λίστα των οχημάτων",
      show: "Προβολή οχήματος",
      edit: "Επεξεργασία οχήματος",
      create: "Δημιουργία οχήματος"
    },
    licenseType: {
      licenseTypes: "Τύποι αδειών",
      licenseType: "Τύπος άδειας",
      title: "Τίτλος",
      isRequired: "Είναι απαραίτητο",
      description: "Περιγραφή",
      relatedResource: "Σχετική οντότητα",
      list: "Λίστα των τύπων άδειας",
      show: "Προβολή τύπου άδειας",
      edit: "Επεξεργασία τύπου άδειας",
      create: "Δημιουργία τύπου άδειας"
    },
    license: {
      licenses: "Άδειες",
      license: "Άδεια",
      expirationDate: "Ημερομηνία λήξης",
      list: "Λίστα των αδειών",
      show: "Προβολή άδειας",
      edit: "Επεξεργασία άδειας",
      create: "Δημιουργία άδειας"

    }
  }
}

export default messages;