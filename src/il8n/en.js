const messages = {
  app: {
    title: 'Truck management system',
    createdAt: "Created at",
    updatedAt: "Updated at",
    id: "Id"
  },
  resources: {
    partner: {
      partners: "Partners",
      partner: "Partner",
      firstName: "First name",
      lastName: "Last name",
      nickName: "Nick name",
      phone: "Phone",
      email: "Email",
      isActive: "Is active",
      address: "Address",
      list: "List partners",
      show: "Show partener",
      edit: "Edit partner",
      create: "Create partner"
    },
    truckOwner: {
      truckOwners: "Truck Owners",
      truckOwner: "Truck Owner",
      list: "List truck owners",
      show: "Show truck owner",
      edit: "Edit truck owner",
      create: "Create truck owner"
    },
    driver: {
      drivers: "Drivers",
      driver: "Driver",
      list: "List drivers",
      show: "Show driver",
      edit: "Edit driver",
      create: "Create driver"
    },
    truck: {
      trucks: "Trucks",
      truck: "Truck",
      owner: "owner",
      driver: "Driver",
      alias: "Alias",
      licenseNumber: "License number",
      list: "List trucks",
      show: "Show truck",
      edit: "Edit truck",
      create: "Create truck"
    },
    licenseType: {
      licenseTypes: "License Types",
      licenseType: "License Type",
      title: "Title",
      isRequired: "Is Required",
      description: "Description",
      relatedResource: "Related Resource",
      list: "List license types",
      show: "Show license type",
      edit: "Edit license type",
      create: "Create license type"
    },
    license: {
      licenses: "Licenses",
      license: "License",
      expirationDate: "Expiration date",
      list: "List licenses",
      show: "Show license",
      edit: "Edit license",
      create: "Create license"
    }
  }
}

export default messages;