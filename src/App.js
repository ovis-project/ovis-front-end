import React from 'react';
import { Admin, Resource } from 'react-admin';
import DirectionsCar from '@material-ui/icons/DirectionsCar';
import Group from '@material-ui/icons/Group';
import Folder from '@material-ui/icons/Folder';
import InsertDriveFile from '@material-ui/icons/InsertDriveFile';
import {partners, truckOwners, drivers, trucks, licenseTypes, licenses} from './resources';
import dataProvider from './config/data-provider';
import i18nProvider from './config/il8n-provider';

const App = () => (
  <Admin title="app.title" dataProvider={dataProvider} i18nProvider={i18nProvider} >
    <Resource name="partners" options={{ label: 'resources.partner.partners' }} icon={Group} {...partners} />
    <Resource name="truck-owners" options={{label: 'resources.truckOwner.truckOwners'}} {...truckOwners} />
    <Resource name="drivers" options={{label: 'resources.driver.drivers'}} {...drivers} />
    <Resource name="trucks" {...trucks} options={{label: 'resources.truck.trucks'}} icon={DirectionsCar} />
    <Resource name="licenses" {...licenses} options={{label: 'resources.license.licenses'}} icon={InsertDriveFile} />
    <Resource name="license-types" options={{label: 'resources.licenseType.licenseTypes'}} {...licenseTypes} icon={Folder} />
  </Admin>
);

export default App;
